library(geosphere)                    

target <- rev(c(57.73098, 28.26725)) ## Kislovo

windspeed <- 5 # m/s
winddir <- 270 # Where wind blows from

distCosine(target, neutraldeploy)
bearing(neutraldeploy, target)

displayosmru(target)

displayosmru(neutraldeploy)

canopymaxspeed <- 10.5

library(rwunderground)
api <- rwunderground::set_api_key(readLines("wundkey"))
rm(api)
set_location(airport_code = "ULOO")

uloo_hourly <- hourly(set_location(airport_code = "ULOO"),
       use_metric = TRUE,
       message = FALSE)

uloo_hourly %>% 
  select(date, wind_spd, wind_dir, rain) %>% 
  filter(date >= ymd_hms("2016-10-02 11:00:00", tz = "MSK"),
         date <= ymd_hms("2016-10-02 13:00:00", tz = "MSK")) 

dir2grad <- function(dir) {
  if(stringr::str_length(dir) > 1L)
    dir <- stringr::str_extract_all(dir, "(N|E|S|W)")
  
  north <- 0 # Suitable for Eastern directions
  if(is.element("W", dir)) north <- 360

  sapply(dir, 
         function(x) {
           switch(x,
            N = north,
            E = 90,
            S = 180,
            W = 270)
         }) #,
         #numeric(1L)
  )
}
